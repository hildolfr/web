var MISSIONS;
fetch('data/missions.json').then(resp => resp.text()).then(data => {
    MISSIONS = JSON.parse(data);
    initExpandables();
    openMenu();
})

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {
        draggable: false,
    });
});

function openMenu() {
    var elem = document.querySelectorAll('.sidenav')[0];
    var instance = M.Sidenav.getInstance(elem);
    instance.open();
}

function openRank(obj) {
    var table = document.getElementById("list");
    table.innerHTML = "";
    
    var rank_text = document.querySelectorAll(".rank-text");
    for (let i = 0; i < rank_text.length; i++) {
        rank_text[i].classList.add("white-text");
        rank_text[i].classList.remove("white");
    }
    
    if (obj.id == "coop") {
        obj.classList.remove("white-text");
        obj.setAttribute("style", "background-color: #fff !important;");
    } else {
        var coop = document.getElementById("coop");
        coop.setAttribute("style", "background-color: #fff;");
        coop.classList.add("white-text");

        obj.classList.remove("white-text");
        obj.classList.add("white");
    }

    var path = document.getElementById("path");
    switch(obj.id) {
    case 'oa':
        addMissions(MISSIONS.offer_a);
        path.innerHTML = "> OFFER > RANK A";
        break;
    case 'ob':
        addMissions(MISSIONS.offer_b);
        path.innerHTML = "> OFFER > RANK B";
        break;
    case 'oc':
        addMissions(MISSIONS.offer_c);
        path.innerHTML = "> OFFER > RANK C";
        break;
    case 'od':
        addMissions(MISSIONS.offer_d);
        path.innerHTML = "> OFFER > RANK D";
        break;
    case 'oe':
        addMissions(MISSIONS.offer_e);
        path.innerHTML = "> OFFER > RANK E";
        break;
    case 'fa':
        addMissions(MISSIONS.free_a);
        path.innerHTML = "> FREE > RANK A";
        break;
    case 'fb':
        addMissions(MISSIONS.free_b);
        path.innerHTML = "> FREE > RANK B";
        break;
    case 'fc':
        addMissions(MISSIONS.free_c);
        path.innerHTML = "> FREE > RANK C";
        break;
    case 'fd':
        addMissions(MISSIONS.free_d);
        path.innerHTML = "> FREE > RANK D";
        break;
    case 'fe':
        addMissions(MISSIONS.free_e);
        path.innerHTML = "> FREE > RANK E";
        break;
    case 'coop':
        addMissions(MISSIONS.co_op);
        path.innerHTML = "> COOP";
        break;
    case 'others':
        addMissions(MISSIONS.others);
        break;
    }
    
    var instances = M.AutoInit();
    initExpandables();
}

function initExpandables() {
    var expandables = document.querySelectorAll('.collapsible.expandable');    
    for (let i = 0; i < expandables.length; i++) {
        var instance = M.Collapsible.init(expandables[i], {
            accordion: false,
            inDuration: 0,
            outDuration: 0,
        });
    }
}

function addMissions(json) {
    var table = document.getElementById("list");
    
    for (let i = 0; i < json.length; i++) {
        var card = document.createElement("div");
        card.className = "card box-wrapper dark";
        
        var card_content1 = document.createElement("div");
        card_content1.className = "card-content transparent white-text";
        card.appendChild(card_content1);
        
        var card_title = document.createElement("span");
        card_title.className = "card-title";
        card_content1.appendChild(card_title);
        
        var card_data = document.createElement("p");
        card_content1.appendChild(card_data);
        
        if (json[i].enemies.length != 0) {
            var card_tabs = document.createElement("div");
            card_tabs.className = "card-tabs white-text";
            card.appendChild(card_tabs);
            
            var card_ul = document.createElement("ul");
            card_ul.className = "tabs tabs-fixed-width transparent white-text";
            card_tabs.appendChild(card_ul);
            
            var card_content2 = document.createElement("div");
            card_content2.className = "card-content card-content2 transparent white-text";
            card.appendChild(card_content2);
            
            var card_li1 = document.createElement("li");
            card_li1.className = "tab";
            card_ul.appendChild(card_li1);
            
            var li1_a = document.createElement("a");
            li1_a.className = "active white-text";
            li1_a.href = `#arsenals${i}`;
            li1_a.innerHTML = "Enemy arsenals";
            card_li1.appendChild(li1_a);
            
            card_content2.appendChild(arsenalColapsable(json[i].enemies, i));
        }

        if (json[i].encounters.rate != 0.0) {
            if (json[i].enemies.length == 0) {
                var card_tabs = document.createElement("div");
                card_tabs.className = "card-tabs white-text";
                card.appendChild(card_tabs);
                
                var card_ul = document.createElement("ul");
                card_ul.className = "tabs tabs-fixed-width transparent white-text";
                card_tabs.appendChild(card_ul);
                
                var card_content2 = document.createElement("div");
                card_content2.className = "card-content card-content2 black white-text";
                card.appendChild(card_content2);
            }

            var card_li2 = document.createElement("li");
            card_li2.className = "tab";
            card_ul.appendChild(card_li2);
            
            var li2_a = document.createElement("a");
            li2_a.className = "white-text";
            li2_a.href = `#encounters${i}`;
            li2_a.innerHTML = `Encounters (${json[i].encounters.rate}%)`;
            card_li2.appendChild(li2_a);
            
            card_content2.appendChild(encounterColapsable(json[i].encounters.arsenals, i));
        }
        
        var goals = "";
        for (let j = 0; j < json[i].goals.length; j++) {
            goals = goals+`<br>- ${json[i].goals[j].text} (${json[i].goals[j].reward}c)`;
        }
        
        var fails = "";
        for (let j = 0; j < json[i].failures.length; j++) {
            fails = fails+`<br>- ${json[i].failures[j].text}`;
        }

        card_title.innerHTML = `<b>${json[i].title}</b>`;
        var line0 = json[i].subtitle;
        var line1 = `<b>CLIENT: </b>${json[i].client}`;
        var line2 = `<b>AREA: </b>${json[i].area}`;
        var line3 = `<b>SUMMARY: </b>${json[i].summary}`;
        
        var line4 = `<b>GOAL(S): </b> ${goals}`;
        var line5 = fails == "" ? "" : `<b>FAILURE: </b> ${fails}`;
        
        card_data.innerHTML = `${line0}<hr>${line1}<br>${line2}<br>${line3}<br><hr>${line4}<br>${line5}`;
        
        table.appendChild(card);
    }
}

function getPartsData(arsenal) {
    var head = formatPart("Head", arsenal.parts.head);
    var body = formatPart("Body", arsenal.parts.body);
    var arm_r = formatPart("Right Arm", arsenal.parts.arm_r);
    var arm_l = formatPart("Left Arm", arsenal.parts.arm_l);
    var legs = formatPart("Legs", arsenal.parts.legs);
    
    var weapon_r = formatPart("Right Weapon", arsenal.weapons.weapon_r);
    var weapon_l = formatPart("Left Weapon", arsenal.weapons.weapon_l);
    var shoulder = formatPart("Shoulder Weapon", arsenal.weapons.shoulder);
    var support = formatPart("Auxiliary", arsenal.weapons.support);
    var pylon_r = formatPart("Right Pylon", arsenal.weapons.pylon_r);
    var pylon_l = formatPart("Left Pylon", arsenal.weapons.pylon_l);
    
    var armor = `${head}<br>${body}<br>${arm_r}<br>${arm_l}<br>${legs}`;
    var weapons = `${weapon_r}<br>${weapon_l}<br>${shoulder}<br>${support}<br>${pylon_r}<br>${pylon_l}`;
    
    var res = `${armor}<br><hr>${weapons}`;
    
    return res;
}

function formatPart(text, part) {
    return `<b>${text}</b>: ${part.name} (${part.droprate}%)`;
}

function arsenalColapsable(ajson, index){
    var arsenals_div = document.createElement("div");
    arsenals_div.id = `arsenals${index}`;
    
    var arsenals_col = document.createElement("ul");
    arsenals_col.className = "collapsible expandable";
    
    for (let j = 0; j < ajson.length; j++) {
        var arsenal_li = document.createElement("li");
        arsenal_li.className = "active";
        
        var arsenal_header = document.createElement("div");
        arsenal_header.className = "collapsible-header";
        var name = ajson[j].outer == "None" ? ajson[j].name : `${ajson[j].outer} - ${ajson[j].name}`;
        arsenal_header.innerHTML = `<b>${name}</b>`;
        arsenal_li.appendChild(arsenal_header);
        
        var arsenal_content = document.createElement("div");
        arsenal_content.className = "collapsible-body";
        arsenal_li.appendChild(arsenal_content);
        
        var arsenal_equip = document.createElement("span");
        arsenal_equip.innerHTML = getPartsData(ajson[j]);
        arsenal_content.appendChild(arsenal_equip);
        
        arsenals_col.appendChild(arsenal_li);
    }
    
    arsenals_div.appendChild(arsenals_col);
    
    return arsenals_div;
}

function encounterColapsable(ajson, index){
    var arsenals_div = document.createElement("div");
    arsenals_div.id = `encounters${index}`;
    
    var arsenals_col = document.createElement("ul");
    arsenals_col.className = "collapsible expandable";
    
    for (let j = 0; j < ajson.length; j++) {
        var arsenal_li = document.createElement("li");
        arsenal_li.className = "active";
        
        var arsenal_header = document.createElement("div");
        arsenal_header.className = "collapsible-header";
        arsenal_header.innerHTML = `<b>${ajson[j].outer} - ${ajson[j].name} (${ajson[j].rate}%)</b>`;
        arsenal_li.appendChild(arsenal_header);
        
        var arsenal_content = document.createElement("div");
        arsenal_content.className = "collapsible-body";
        arsenal_li.appendChild(arsenal_content);
        
        var arsenal_equip = document.createElement("span");
        arsenal_equip.innerHTML = getPartsData(ajson[j]);
        arsenal_content.appendChild(arsenal_equip);
        
        arsenals_col.appendChild(arsenal_li);
    }
    
    arsenals_div.appendChild(arsenals_col);
    
    return arsenals_div;
}
