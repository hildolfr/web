var COLLECTIONS;
fetch('data/collection.json').then(resp => resp.text()).then(data => {
    COLLECTIONS = JSON.parse(data);
    initExpandables();
    openMenu();
})

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {
        draggable: false,
    });
});

function openMenu() {
    var elem = document.querySelectorAll('.sidenav')[0];
    var instance = M.Sidenav.getInstance(elem);
    instance.open();
}

function openRank(obj) {
    var table = document.getElementById("list");
    table.innerHTML = "";
    
    var rank_text = document.querySelectorAll(".rank-text");
    for (let i = 0; i < rank_text.length; i++) {
        rank_text[i].classList.add("white-text");
        rank_text[i].classList.remove("white");
    }
    
    obj.classList.remove("white-text");
    obj.classList.add("white");

    var path = document.getElementById("path");
    switch(obj.id) {
    case 'patterns':
        addCollection(COLLECTIONS.patterns);
        path.innerHTML = "> PATTERNS";
        break;
    case 'decals':
        addCollection(COLLECTIONS.decals);
        path.innerHTML = "> DECALS";
        break;
    case 'titles':
        addCollection(COLLECTIONS.titles);
        path.innerHTML = "> TITLES";
        break;
    }
    
    var instances = M.AutoInit();
    initExpandables();
}

function addCollection(json) {
    var table = document.getElementById("list");
    
    for (let i = 0; i < json.length; i++) {
        var col_ul = document.createElement("ul");
        col_ul.className = "collection";
        
        var col_itm_li = document.createElement("li");
        col_itm_li.className = "collection-item white-text";
        col_ul.appendChild(col_itm_li);
        
        var col_title = document.createElement("span");
        col_title.className = "title";
        col_title.innerHTML = json[i].name;
        col_itm_li.appendChild(col_title);
        
        var col_line = document.createElement("p");
        col_line.className = "line";
        col_line.innerHTML = `Condition: ${json[i].condition}`;
        col_itm_li.appendChild(col_line);
        
        table.appendChild(col_ul);
    }
}

function initExpandables() {
    var expandables = document.querySelectorAll('.collapsible.expandable');    
    for (let i = 0; i < expandables.length; i++) {
        var instance = M.Collapsible.init(expandables[i], {
            accordion: false,
            inDuration: 0,
            outDuration: 0,
        });
    }
}
