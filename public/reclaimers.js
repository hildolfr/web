var COLLECTIONS;
fetch('data/reclaimers.json').then(resp => resp.text()).then(data => {
    RECLAIMERS = JSON.parse(data);
    loadGroupList();
    initExpandables();
    openMenu();
})

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {
        draggable: false,
    });
});

function openMenu() {
    var elem = document.querySelectorAll('.sidenav')[0];
    var instance = M.Sidenav.getInstance(elem);
    instance.open();
}

function openGroup(key) {
    obj = document.getElementById(key);
    var table = document.getElementById("list");
    table.innerHTML = "";
    
    var group_text = document.querySelectorAll(".group-text");
    for (let i = 0; i < group_text.length; i++) {
        group_text[i].classList.add("white-text");
        group_text[i].classList.remove("white");
    }
    
    obj.classList.remove("white-text");
    obj.classList.add("white");

    var path = document.getElementById("path");
    addMembers(RECLAIMERS[key].members);
    path.innerHTML = `> ${RECLAIMERS[key].name.toUpperCase()}`;
    
    var instances = M.AutoInit();
    initExpandables();
}

function addMembers(json) {
    var table = document.getElementById("list");
    
    for (let i = 0; i < json.length; i++) {
        var col_ul = document.createElement("ul");
        col_ul.className = "collection";
        
        var col_itm_li = document.createElement("li");
        col_itm_li.className = "collection-item white-text";
        col_ul.appendChild(col_itm_li);
        
        var col_title = document.createElement("span");
        col_title.className = "title";
        col_title.innerHTML = `<b>${json[i].outer.name}</b>`;
        col_itm_li.appendChild(col_title);
        
        var col_line = document.createElement("p");
        col_line.className = "line";
        
        var skilllist = listSkills(json[i].outer.skills);
        var skilltext = skilllist ? `<hr><b>Special skills:</b><br>${skilllist}` : "";
        
        col_line.innerHTML = `<hr>${json[i].outer.description}${skilltext}`;
        col_itm_li.appendChild(col_line);
        
        var col_line2 = document.createElement("p");
        col_line2.className = "line";
        col_line2.innerHTML = `<hr><b>Arsenal</b>: ${json[i].arsenal.name}<br>${json[i].arsenal.description}`;
        col_itm_li.appendChild(col_line2);
        
        table.appendChild(col_ul);
    }
}

function listSkills(array) {
    var res = "";
    for (let i = 0; i < array.length; i++) {
        var type = array[i].type;
        var value = array[i].value;
        var compl = value ? `- ${type} (${value}%)` : `- ${type}`
        res = res + `${compl}<br>`;
    }
    
    return res;
}

function loadGroupList() {
    var list = document.getElementById("group-list");
    
    Object.keys(RECLAIMERS).forEach(function(key,index) {
        var gli = document.createElement("li");
        gli.className = "group";
        
        var ndiv = document.createElement("div");
        ndiv.className = "group-text white-text";
        ndiv.id = key;
        ndiv.innerHTML = RECLAIMERS[key].name;
        
        ndiv.addEventListener("click", function(e) {
            openGroup(key);
        }, false);
        
        gli.appendChild(ndiv);
        
        list.appendChild(gli);
    });
}

function initExpandables() {
    var expandables = document.querySelectorAll('.collapsible.expandable');    
    for (let i = 0; i < expandables.length; i++) {
        var instance = M.Collapsible.init(expandables[i], {
            accordion: false,
            inDuration: 0,
            outDuration: 0,
        });
    }
}
