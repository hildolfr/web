![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

This is the website with the processed data, feel free to make a merge request if you want to contribute (or hit me up in discord Hario#0540).

---

Tools used for datamining DxM:
- [hactool][hactool]
- [quickbms][quickbms]
- [UE Viewer/umodel][umodel]
- [locres2csv][locres2csv]
- [UAssetParser][uaparser]
- [JohnWickParse][jwparse] (same functionality as above, has issues with duplicated keys)

[hactool]: https://github.com/SciresM/hactool/
[quickbms]: https://aluigi.altervista.org/quickbms.htm
[umodel]: https://www.gildor.org/en/projects/umodel
[locres2csv]: https://drive.google.com/file/d/1-ftM3xAukoogkU5SmNKmsXYLA4b6omgJ/view
[jwparse]: https://github.com/SirWaddles/JohnWickParse/
[uaparser]: https://github.com/healingbrew/UAssetParser